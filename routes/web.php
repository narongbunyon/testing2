<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileCotrollers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::get('/testing', function () {
    return view('testing');
});


Route::get('/profiles',"ProfileController@lists");


// Route::get('profiles', [ProfileController::class, 'lists']);


